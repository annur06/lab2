FROM openjdk:17

WORKDIR /

COPY . /

RUN javac Project.java

ENTRYPOINT ["java", "Project"]
